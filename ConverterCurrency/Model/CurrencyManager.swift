//
//  CurrencyManager.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/22/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//
import Foundation

class CurrencyManager {
    func getCurrency(complition: @escaping (([Currency]) -> Void)) {
        let url = URL(string: "https://www.nbrb.by/api/exrates/rates?periodicity=0")!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    complition([])
                    return
            }
            
            do{
                let currencyArray = try JSONDecoder().decode([Currency].self, from: dataResponse)
                DispatchQueue.main.async {
                    complition(currencyArray)
                }
            }catch let parsingError{
                print("Error", parsingError)
            }
        }.resume()
    }
    
    func getCurrencyChangeInformation(curId: Int, startDate: String, endDate: String, complition: @escaping (([Currency]) -> Void)) {
        let url = URL(string: "https://www.nbrb.by/api/ExRates/Rates/Dynamics/\(curId)?startDate=\(startDate)&endDate=\(endDate)")!
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    complition([])
                    return
            }
            
            do{
                let currencyArray = try JSONDecoder().decode([Currency].self, from: dataResponse)
                DispatchQueue.main.async {
                    complition(currencyArray)
                }
            }catch let parsingError{
                print("Error", parsingError)
            }
        }.resume()
    }
}
