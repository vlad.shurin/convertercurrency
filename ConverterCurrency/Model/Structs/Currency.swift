//
//  Currency.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/22/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

struct Currency: Decodable {
    var Cur_ID: Int
    var Date: String
    var Cur_OfficialRate: Double
    var Cur_Abbreviation: String?
    var Cur_Name: String?
}
