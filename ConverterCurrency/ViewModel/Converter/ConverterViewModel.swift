//
//  ConverterViewModel.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/28/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//
import Charts
import Foundation

class ConverterViewModel {
    var name: String!
    var officialRate: Double!
    var currencyValue: String! = "1"
    var abbreviation: String!
    var bynValue: String!
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatusClosure?()
        }
    }
    private var curId: Int!
    private var cellsArray = [ListCurrencyDetailCellViewModel]()
    var chartData = [ChartDataEntry]()
    var chartXAxisLabels = [String]()
    
    var updateBynValueClosure: (()->())?
    var updateCurrencyValueClosure: (()->())?
    var updateCurrencyChangeTableClosure: (()->())?
    var updateLoadingStatusClosure: (()->())?
    var updateChartClosure: (()->())?

    private weak var currencyManager: CurrencyManager!

    required init(currency: Currency, currencyManager: CurrencyManager) {
        self.officialRate = currency.Cur_OfficialRate
        self.bynValue = String(currency.Cur_OfficialRate)
        self.name = currency.Cur_Name
        self.abbreviation = (currency.Cur_Abbreviation ?? "") + " -"
        self.curId = currency.Cur_ID
        self.currencyManager = currencyManager
        getCurrencyChangeInformation()
    }
    
    func convert(value: String, isByn: Bool) {
        if let value = Double(value.replacingOccurrences(of: ",", with: ".")) {
            if isByn {
                self.currencyValue = String(format: "%.2f", value / officialRate)
                updateCurrencyValueClosure?()
            } else {
                self.bynValue = String(format: "%.2f", value * officialRate)
                updateBynValueClosure?()
            }
        }
    }
    
    func getCurrencyChangeInformation() {
        isLoading = true
        cellsArray.removeAll()
        chartData.removeAll()
        chartXAxisLabels.removeAll()
        let datesInterval = getDatesInterval()
        currencyManager.getCurrencyChangeInformation(curId: curId, startDate: datesInterval.0, endDate: datesInterval.1) { (currencyArray) in
            var elementNumber = 0
            for currencyObject in currencyArray {
                self.cellsArray.append(ListCurrencyDetailCellViewModel(currency: currencyObject, currentRate: self.officialRate))
                self.chartData.append(ChartDataEntry(x: Double(elementNumber), y: currencyObject.Cur_OfficialRate))
                self.chartXAxisLabels.append(self.parseDateTime(datetime: currencyObject.Date))
                elementNumber += 1
            }
            self.cellsArray = self.cellsArray.reversed()
            self.isLoading = false
            self.updateCurrencyChangeTableClosure?()
            self.updateChartClosure?()
        }
    }
    
    func getDatesInterval() -> (String, String) {
        var dayComponent = DateComponents()
        let theCalendar = Calendar.current
        let endDate = theCalendar.date(byAdding: dayComponent, to: Date())
        dayComponent.day = -30
        let startDate = theCalendar.date(byAdding: dayComponent, to: Date())
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return (formatter.string(from: startDate!), formatter.string(from: endDate!))
    }
    
    func numberOfCurrency() -> Int {
        return cellsArray.count
    }
    
    func cellViewModel(index: Int) -> ListCurrencyDetailCellViewModel {
        return cellsArray[index]
    }
    
    func parseDateTime(datetime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: datetime)!
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d MMMM"
        return dateFormatter.string(from: date)
    }
}

