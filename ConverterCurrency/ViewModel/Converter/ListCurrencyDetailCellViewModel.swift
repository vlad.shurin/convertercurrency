//
//  ListCurrencyDetailCellViewModel.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/29/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import Foundation

enum enumColor {
    case standart
    case green
    case red
}

class ListCurrencyDetailCellViewModel {
    var date: String!
    var value: String!
    var color: enumColor!

    required init(currency: Currency, currentRate: Double) {
        self.date = parseDateTime(datetime: currency.Date)
        self.value = String(currency.Cur_OfficialRate)
        self.color = getColor(rate: currency.Cur_OfficialRate, currentRate: currentRate)
    }
    
    func getColor(rate: Double, currentRate: Double) -> enumColor {
        if rate < currentRate {
            return .red
        } else if rate > currentRate {
            return .green
        } else {
            return .standart
        }
    }
    
    func parseDateTime(datetime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: datetime)!
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "EEEE, d MMMM yyyy"
        return dateFormatter.string(from: date)
    }
}
