//
//  ListCurrencyCellViewModel.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/24/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import Foundation

class ListCurrencyCellViewModel {
    var abbreviation: String!
    var name: String!
    var value: String!

    required init(currency: Currency) {
        self.abbreviation = currency.Cur_Abbreviation
        self.name = currency.Cur_Name
        self.value = String(currency.Cur_OfficialRate)
    }
}
