//
//  ListCurrencyViewModel.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/22/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

class ListCurrencyViewModel {
    private var currencyManager: CurrencyManager!
    private var currencyArray: [Currency]!
    private var cellsArray = [ListCurrencyCellViewModel]()
    var converterViewModel: ConverterViewModel!
    
    var reloadTableViewClosure: (()->())?
    var updateLoadingStatusClosure: (()->())?
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatusClosure?()
        }
    }

    func fetchCurrency() {
        self.isLoading = true
        cellsArray.removeAll()
        currencyManager.getCurrency { (currencyArray) in
            self.currencyArray = currencyArray
            for currencyObject in currencyArray {
                self.cellsArray.append(ListCurrencyCellViewModel(currency: currencyObject))
            }
            self.reloadTableViewClosure?()
            self.isLoading = false
        }
    }
    
    func numberOfCurrency() -> Int {
        return cellsArray.count
    }
    
    func cellViewModel(index: Int) -> ListCurrencyCellViewModel {
        return cellsArray[index]
    }
    
    func converterViewModel(index: Int) -> ConverterViewModel {
        self.converterViewModel = ConverterViewModel(currency: currencyArray[index], currencyManager: currencyManager)
        return self.converterViewModel
    }
    
    required init() {
        self.currencyManager = CurrencyManager()
    }
}
