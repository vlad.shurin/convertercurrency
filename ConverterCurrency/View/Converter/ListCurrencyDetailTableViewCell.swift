//
//  ListCurrencyDetailTableViewCell.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/29/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import UIKit

class ListCurrencyDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    static let cellIdentifier = "ListCurrencyDetailTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ListCurrencyDetailTableViewCell", bundle: nil)
    }
    
    public func configure(with viewModel: ListCurrencyDetailCellViewModel) {
        dateLabel.text = viewModel.date
        valueLabel.text = viewModel.value
        switch viewModel.color {
        case .red:
            valueLabel.textColor = UIColor.red
            break
        case .green:
            valueLabel.textColor = UIColor.green
            break
        default:
            valueLabel.textColor = traitCollection.userInterfaceStyle == .light ? UIColor.black : UIColor.white
            break
        }
    }
}
