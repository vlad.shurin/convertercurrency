//
//  ConverterViewController.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/28/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import Charts
import UIKit

class ConverterViewController: UIViewController {

    @IBOutlet weak var chartView: UIView!
    @IBOutlet weak var abbreviationLabel: UILabel!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var bynTextField: UITextField!
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    weak var viewModel: ConverterViewModel!
    lazy var lineChartView: LineChartView = {
        var lineChart = LineChartView()
        lineChart.frame = CGRect(x: 0, y: 0, width: self.chartView.frame.size.width, height: self.chartView.frame.size.height)
        lineChart.center = self.chartView.center
        lineChart.rightAxis.enabled = false
        lineChart.leftAxis.setLabelCount(4, force: false)
        lineChart.leftAxis.labelPosition = .insideChart
        lineChart.xAxis.labelPosition = .bottom
        lineChart.animate(xAxisDuration: 1)
        return lineChart
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initTableView()
        initClosures()
    }
    
    func initView() {
        abbreviationLabel.text = viewModel.abbreviation
        currencyTextField.text = viewModel.currencyValue
        currencyTextField.keyboardType = .decimalPad
        bynTextField.text = viewModel.bynValue
        bynTextField.keyboardType = .decimalPad
        currencyNameLabel.text = viewModel.name
        currencyTextField.addTarget(self, action: #selector(self.currencyTextFieldDidChange(_:)), for: .editingChanged)
        bynTextField.addTarget(self, action: #selector(self.bunTextFieldDidChange), for: .editingChanged)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.delegate = self
        view.addGestureRecognizer(tap)
    }
    
    func initTableView() {
        tableView.rowHeight = 65
        tableView.register(ListCurrencyDetailTableViewCell.nib(), forCellReuseIdentifier: ListCurrencyDetailTableViewCell.cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func initClosures() {
        viewModel.updateBynValueClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.bynTextField.text = self?.viewModel.bynValue
            }
        }
        
        viewModel.updateCurrencyValueClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.currencyTextField.text = self?.viewModel.currencyValue
            }
        }
        
        viewModel.updateCurrencyChangeTableClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateChartClosure = {  [weak self] () in
            DispatchQueue.main.async {
                self?.createChart()
            }
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func currencyTextFieldDidChange(_ textField: UITextField) {
        viewModel.convert(value: textField.text ?? "", isByn: false)
    }
    
    @objc func bunTextFieldDidChange(_ textField: UITextField) {
        viewModel.convert(value: textField.text ?? "", isByn: true)
    }
}

extension ConverterViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCurrency()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListCurrencyDetailTableViewCell.cellIdentifier) as! ListCurrencyDetailTableViewCell
        cell.configure(with: viewModel.cellViewModel(index: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = viewModel.numberOfCurrency() - indexPath.row - 1
        lineChartView.highlightValue(Highlight(x: viewModel.chartData[index].x, y: viewModel.chartData[index].x, dataSetIndex: 0))
    }
}

extension ConverterViewController: ChartViewDelegate {
    func createChart() {
        lineChartView.delegate = self
        chartView.addSubview(lineChartView)
    
        let gradientColors = [UIColor.green.cgColor, UIColor.blue.cgColor] as CFArray
        let colorLocations:[CGFloat] = [1.0, 0.0]
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)
        
        let set = LineChartDataSet(entries: viewModel.chartData, label: "")
        let colorChart = traitCollection.userInterfaceStyle == .light ? UIColor.black : UIColor.white
        set.circleRadius = 2
        set.setCircleColor(colorChart)
        set.mode = .cubicBezier
        set.setColor(colorChart)
        set.fill = Fill(linearGradient: gradient!, angle: 90)
        set.fillAlpha = 0.6
        set.drawFilledEnabled = true
        let data = LineChartData(dataSet: set)
        lineChartView.data = data
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: viewModel.chartXAxisLabels)
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        self.tableView.selectRow(at: IndexPath(row: viewModel.numberOfCurrency() - Int(entry.x) - 1, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.middle)
    }
}

extension ConverterViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.isDescendant(of: self.tableView) {
            return false
        }
        return true
    }
}
