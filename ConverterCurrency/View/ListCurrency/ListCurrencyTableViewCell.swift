//
//  ListCurrencyTableViewCell.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/28/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import UIKit

class ListCurrencyTableViewCell: UITableViewCell {
    @IBOutlet weak var abbreviationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    static let cellIdentifier = "ListCurrencyTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ListCurrencyTableViewCell", bundle: nil)
    }
    
    public func configure(with viewModel: ListCurrencyCellViewModel) {
        abbreviationLabel.text = viewModel.abbreviation
        nameLabel.text = viewModel.name
        valueLabel.text = viewModel.value
    }
}
