//
//  ViewController.swift
//  ConverterCurrency
//
//  Created by VLADISLAV on 10/21/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import UIKit

class ListCurrencyViewController: UIViewController {
    lazy var viewModel: ListCurrencyViewModel = {
        return ListCurrencyViewModel()
    }()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    var segueIdenitifer = "showConverter"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        initViewModel()
    }

    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ListCurrencyTableViewCell.nib(), forCellReuseIdentifier: ListCurrencyTableViewCell.cellIdentifier)
        tableView.rowHeight = 70
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewModel.fetchCurrency()
    }
    
    func initViewModel() {
        viewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel.updateLoadingStatusClosure = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                isLoading ? self?.refreshControl.beginRefreshing() : self?.refreshControl.endRefreshing()
            }
        }
        viewModel.fetchCurrency()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdenitifer {
            if let converterController = segue.destination as? ConverterViewController, let index = sender as? Int {
                converterController.viewModel = self.viewModel.converterViewModel(index: index)
            }
        }
    }
}

extension ListCurrencyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCurrency()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListCurrencyTableViewCell.cellIdentifier, for: indexPath) as! ListCurrencyTableViewCell        
        cell.configure(with: viewModel.cellViewModel(index: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: segueIdenitifer, sender: indexPath.row)
    }
}
