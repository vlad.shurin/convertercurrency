//
//  CurrencyWidget.swift
//  CurrencyWidget
//
//  Created by VLADISLAV on 11/4/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import WidgetKit
import SwiftUI
import Foundation

struct CurrencyWidgetEntryView : View {
    var entry: Provider.Entry
    
    @Environment(\.widgetFamily) var family

    @ViewBuilder
    var body: some View {
        switch family {
        case .systemSmall:
            SmallView(_currency: entry.currency.first!)
        case .systemMedium:
            MediumView(_listCurrency: entry.currency, _date: entry.dateString)
        default:
            fatalError()
        }
    }
}

@main
struct CurrencyWidget: Widget {
    let kind: String = "CurrencyWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            CurrencyWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Курсы")
        .description("Виджет с актуальными курсами")
        .supportedFamilies([.systemSmall, .systemMedium])
    }
}

struct CurrencyWidget_Previews: PreviewProvider {
    static var previews: some View {
        CurrencyWidgetEntryView(entry: CurrencyEntry.mockCurrencyEnty())
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
