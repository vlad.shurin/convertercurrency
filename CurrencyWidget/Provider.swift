//
//  Provider.swift
//  CurrencyWidgetExtension
//
//  Created by VLADISLAV on 11/4/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import WidgetKit
import Foundation

struct Provider: TimelineProvider {
    var loader: CurrencyManager = CurrencyManager()

    typealias Entry = CurrencyEntry
    
    func placeholder(in context: Context) -> CurrencyEntry {
        CurrencyEntry.mockCurrencyEnty()
    }

    func getSnapshot(in context: Context, completion: @escaping (CurrencyEntry) -> ()) {
        let entry = CurrencyEntry.mockCurrencyEnty()
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        loader.getCurrency { (arrayCurrency) in
            let date = Date()
            let entry = CurrencyEntry(date: date,
                                      dateString: getCurrentDateString(),
                                      currency: filterArrayCurrency(array: arrayCurrency))
            let refreshDate = Calendar.current.date(byAdding: .minute, value: 1, to: date)!
            let timeline = Timeline(entries: [entry], policy: .after(refreshDate))
            completion(timeline)
        }
    }
    
    func filterArrayCurrency(array: [Currency]) -> [Currency] {
        return array.filter { currency in
            return currency.Cur_Abbreviation == "USD" || currency.Cur_Abbreviation == "EUR" || currency.Cur_Abbreviation == "RUB"
        }        
    }
    
    func getCurrentDateString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d MMMM HH:mm"
        return dateFormatter.string(from: date)
    }
}
