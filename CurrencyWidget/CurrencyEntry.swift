//
//  CurrencyEntry.swift
//  CurrencyWidgetExtension
//
//  Created by VLADISLAV on 11/4/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import WidgetKit

struct CurrencyEntry: TimelineEntry {
    let date: Date
    let dateString: String
    let currency: [Currency]
    
    static func mockCurrencyEnty() -> CurrencyEntry {
        return CurrencyEntry(date: Date(), dateString: "25 октября", currency: [
                                Currency(Cur_ID: 124, Date: "", Cur_OfficialRate: 2.6563, Cur_Abbreviation: "USD", Cur_Name: "Доллар"),
                                Currency(Cur_ID: 125, Date: "", Cur_OfficialRate: 3.0983, Cur_Abbreviation: "EUR", Cur_Name: "Евро"),
                                Currency(Cur_ID: 126, Date: "", Cur_OfficialRate: 3.3211, Cur_Abbreviation: "RUB", Cur_Name: "Российский рубль")])
    }
}
