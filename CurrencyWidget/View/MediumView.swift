//
//  MediumView.swift
//  CurrencyWidgetExtension
//
//  Created by VLADISLAV on 11/4/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import SwiftUI
import WidgetKit

struct MediumView: View {
    private var listCurrency: [Currency]
    private var date: String
    
    init(_listCurrency: [Currency], _date: String) {
        self.listCurrency = _listCurrency
        self.date = _date
    }
    
    var body: some View {
        VStack(alignment:.leading){
            Text("Курсы на \(date)")
                .font(Font.headline)
                .foregroundColor(Color.gray)

            ForEach(listCurrency, id: \.Cur_ID){ item in
                HStack{
                    RoundedRectangle(cornerRadius: 1)
                        .fill(Color.green)
                        .frame(width: 2, height: 20)
                    Text(item.Cur_Abbreviation!).font(.callout)
                    Spacer()
                    Text(String(item.Cur_OfficialRate))
                        .font(.callout)
                        .foregroundColor(Color.primary)

                }
                Divider()
            }
        }.padding()
    }
}

struct MediumView_Previews: PreviewProvider {
    static var previews: some View {
        MediumView(_listCurrency: [
                    Currency(Cur_ID: 124, Date: "25.10.2020", Cur_OfficialRate: 2.6563, Cur_Abbreviation: "USD", Cur_Name: "Доллар"),
                    Currency(Cur_ID: 125, Date: "", Cur_OfficialRate: 3.0983, Cur_Abbreviation: "EUR", Cur_Name: "Евро"),
                    Currency(Cur_ID: 126, Date: "", Cur_OfficialRate: 3.3211, Cur_Abbreviation: "RUB", Cur_Name: "Российский рубль")], _date: "25 октября")
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
