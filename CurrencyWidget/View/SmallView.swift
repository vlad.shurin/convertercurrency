//
//  SmallView.swift
//  CurrencyWidgetExtension
//
//  Created by VLADISLAV on 11/4/20.
//  Copyright © 2020 VLADISLAV. All rights reserved.
//

import SwiftUI
import WidgetKit

struct SmallView: View {
    private var currency: Currency
    
    init(_currency: Currency) {
        self.currency = _currency
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10, content: {
            Text(currency.Cur_Abbreviation ?? "").font(Font.headline).foregroundColor(Color.primary)
            Text(currency.Cur_Name ?? "").font(Font.subheadline).foregroundColor(Color.gray)
            
            HStack {
                RoundedRectangle(cornerRadius: 1)
                    .fill(Color.green)
                    .frame(width: 2, height: 37)
                Text(String(currency.Cur_OfficialRate)).font(.title)
            }
        })
    }
}

struct SmallView_Previews: PreviewProvider {
    static var previews: some View {
        SmallView(_currency: Currency(Cur_ID: 124, Date: "25.10.2020", Cur_OfficialRate: 2.6563, Cur_Abbreviation: "USD", Cur_Name: "Доллар"))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
